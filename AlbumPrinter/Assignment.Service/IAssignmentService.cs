﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.Service
{
    public interface IAssignmentService<T> : IDisposable where T : class
    {
        long GetCount();
        long GetCount(System.Linq.Expressions.Expression<Func<T, bool>> predicate);
        IList<T> GetAll(bool withTracking);
        IList<T> GetAll();
        IList<T> GetAll(System.Linq.Expressions.Expression<Func<T, bool>> predicate, bool withTracking);
        IList<T> GetAll(System.Linq.Expressions.Expression<Func<T, bool>> predicate);
        T Add(T entity);
        void Delete(object key);
        void Delete(System.Linq.Expressions.Expression<Func<T, bool>> predicate);
        void Update(T entity);
        IList<T> Find(System.Linq.Expressions.Expression<Func<T, bool>> predicate, bool withTracking);
        IList<T> Find(System.Linq.Expressions.Expression<Func<T, bool>> predicate);
        void Save();
    }
}

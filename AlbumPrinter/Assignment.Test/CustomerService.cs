﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Assignment.Test
{
    /// <summary>
    /// Summary description for CustomerService
    /// </summary>
    [TestClass]
    public class CustomerService
    {
        Business.Interface.ICustomerService customerService;
        public CustomerService()
        {
            customerService = new Business.CustomerService(new Domain.InMemoryConcreat.AssignmentDataContex());
        }

        [TestMethod]
        [ExpectedException(typeof(Common.Exceptions.DuplicateEntity))]
        public void Duplicate_Email_Should_Fail()
        {
            // Arrange
            customerService.NewCustomer(new Domain.Customer { Name = "b" , Family = "b" , Email = "behnam.bagheri@gmail.com"});

            // Act
            customerService.NewCustomer(new Domain.Customer { Name = "b", Family = "b", Email = "behnam.bagheri@gmail.com" });

            // Assert 
        }
    }
}

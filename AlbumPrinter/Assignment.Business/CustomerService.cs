﻿using Assignment.Business.Interface;
using Assignment.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment.Domain;

namespace Assignment.Business
{
    public class CustomerService : Assignment.Service.AssignmentService<Domain.Customer>, ICustomerService
    {
        public CustomerService(IUnitOfWork unitofWork) : base(unitofWork)
        {
        }

        public void NewCustomer(Customer customer)
        {
            // check if customer email exsists
            if (Find(p => p.Email.Equals(customer.Email)).Count > 0)
            {
                throw new Common.Exceptions.DuplicateEntity("Duplicat customer is not allowed!");
            }

            Add(customer);
        }
    }
}

﻿using Assignment.Business.Interface;
using Assignment.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.Business
{
    public class OrderService : Assignment.Service.AssignmentService<Domain.Order>, IOrderService
    {
        public OrderService(IUnitOfWork unitofWork) : base(unitofWork)
        {
        }
    }
}

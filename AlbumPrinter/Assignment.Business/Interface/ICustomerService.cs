﻿using Assignment.Domain;
using Assignment.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.Business.Interface
{
    public interface ICustomerService : IAssignmentService<Domain.Customer>
    {
        void NewCustomer(Customer customer);
    }
}

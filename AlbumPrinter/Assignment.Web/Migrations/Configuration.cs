namespace Assignment.Domain.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Assignment.Domain.EFConcreat.AssignmentDataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Assignment.Domain.EFConcreat.AssignmentDataContext context)
        {
            var customer1 = new Customer { Name = "behnam", Family = "bagheri", Email = "behnam.bagheri@gmail.com" };
            var customer2 = new Customer { Name = "jack", Family = "Janson", Email = "jack@gmail.com" };
            context.Customers.Add(customer1);
            context.Customers.Add(customer2);

            context.SaveChanges();

            context.Orders.Add(new Order { CreatedDate = DateTime.Now.AddMinutes(-100), Customer = customer1, Price = 100 });
            context.Orders.Add(new Order { CreatedDate = DateTime.Now.AddDays(-1), Customer = customer1, Price = 32 });

            context.Orders.Add(new Order { CreatedDate = DateTime.Now.AddMinutes(-100), Customer = customer2, Price = 430 });
            context.Orders.Add(new Order { CreatedDate = DateTime.Now.AddDays(-1), Customer = customer2, Price = 123 });

            context.SaveChanges();
        }
    }
}

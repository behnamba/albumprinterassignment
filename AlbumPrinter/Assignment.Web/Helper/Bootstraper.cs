﻿using Assignment.Business;
using Assignment.Business.Interface;
using Assignment.Common.Data;
using Assignment.Domain.EFConcreat;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment.Web.Helper
{
    public class Bootstraper
    {
        static IKernel _kernel = new StandardKernel();

        public Bootstraper()
        {

        }

        public static void InitialDependencyMappings()
        {
            _kernel.Bind<IUnitOfWork>().To<AssignmentDataContext>();

            _kernel.Bind<IOrderService>().To<OrderService>();
            _kernel.Bind<ICustomerService>().To<CustomerService>();
        }

        public static IOrderService OrderService
        {
            get
            {
                return _kernel.Get<IOrderService>();
            }
        }

        public static ICustomerService CustomerService
        {
            get
            {
                return _kernel.Get<ICustomerService>();
            }
        }
    }
}
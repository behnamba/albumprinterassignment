﻿using Assignment.Domain;
using Assignment.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Assignment.Web.Controllers
{
    public class OrderController : ApiController
    {
        [HttpGet]
        public List<OrderViewModel> Get(int custumerId)
        {
            // get customer 
            var customer = Helper.Bootstraper.CustomerService.Find(p => p.Id == custumerId).SingleOrDefault();

            if (customer != null)
            {

                // get orders  
                return Helper.Bootstraper.OrderService.Find(p => p.Customer.Id == custumerId).Select(p =>
                new OrderViewModel
                {
                    CreatedDate = p.CreatedDate,
                    Price = p.Price,
                    Id = p.Id
                })
                .ToList();

            }

            return null;
        }
    }
}

﻿using Assignment.Domain;
using Assignment.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Assignment.Web.Controllers
{
    public class CustomerController : ApiController
    {
        [HttpGet]
        public List<CustomerViewModel> Get()
        {
            // get data 
            var customerModel = Helper.Bootstraper.CustomerService;
            return customerModel.GetAll(false).Select(p => new CustomerViewModel
            {
                Email = p.Email,
                Name = p.Name,
                Family = p.Family,
                Id = p.Id
            }).ToList();
        }
    }
}

﻿"use strict";

angular.module("assignmentApp")
    .factory("assignmentBackendService", ["$http", function ($http) {
        var catBackendService = {};

        catBackendService.getCustomers = function (canceler) {
            return $http.get("v1/customer", {
                params: {                    
                }, withCredentials: true, timeout: canceler.promise
            });
        }

        catBackendService.getOrders = function (custumerId, canceler) {
            return $http.get("v1/order?custumerId=" + custumerId, {
                params: {
                    
                }, withCredentials: true, timeout: canceler.promise
            });
        }

        return catBackendService;
    }]);
﻿"use strict";

angular.module("assignmentApp").controller("assignmentMainController", ["$scope", "$q", "$log", "$filter", "assignmentBackendService",
        function ($scope, $q, $log, $filter, assignmentBackendService) {
            var main = this;
            main.title = "Album Printer Assignment :-)";

            // to optimize the performance of the request 
            var customerCanceler = $q.defer();
            var orderCanceler = $q.defer();

            // show loading 
            main.customersLoading = true;

            // to handel showing the order list
            main.cusomerSelected = false;

            // customer grid config
            main.customerGridOptions = {
                data: null,
                enableRowHeaderSelection: false,
                multiSelect: false,
                onRegisterApi: function (gridApi) {
                    gridApi.selection.on.rowSelectionChanged(null, function (row) {
                        // show order list
                        main.cusomerSelected = true;

                        // load Order data
                        loadOrders(row.entity.Id);
                    });
                }
            };

            // customer grid config
            main.orderGridOptions = {
                data: null,
                enableRowHeaderSelection: false,

            };

            // load customers
            assignmentBackendService.getCustomers(customerCanceler)
                .success(function (data) {
                    main.customerGridOptions.data = data;
                    main.customersLoading = false;
                })
                .error(function (data) {
                    alert(data.Message);
                    main.customersLoading = false;
                });

            function loadOrders(customerId) {
                // show loading 
                main.ordersLoading = true;

                assignmentBackendService.getOrders(customerId, orderCanceler)
                .success(function (data) {
                    main.orderGridOptions.data = data;
                    main.ordersLoading = false;
                })
                .error(function (data) {
                    alert(data.Message);
                    main.ordersLoading = false;
                });
            }

            $scope.$on('$destroy', function () {
                customerCanceler.resolve();
                orderCanceler.resolve();
            });
        }]);

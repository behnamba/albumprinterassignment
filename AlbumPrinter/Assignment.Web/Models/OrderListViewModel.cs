﻿using Assignment.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment.Web.Models
{
    public class OrderViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public decimal Price { get; set; }
    }
}
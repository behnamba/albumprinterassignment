﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment.Web.Models
{
    public class CustomerViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Family { get; set; }
        public string Email { get; set; }
    }
}
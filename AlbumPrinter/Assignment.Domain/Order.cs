﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.Domain
{
    public class Order
    {
        public int Id { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        [Required]
        [Range(0.0, Double.MaxValue)]
        public Decimal Price { get; set; }

        public Customer Customer { get; set; }
    }
}

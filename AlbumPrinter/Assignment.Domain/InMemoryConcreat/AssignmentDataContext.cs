﻿using Assignment.Common.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Infrastructure;

namespace Assignment.Domain.InMemoryConcreat
{
    public class AssignmentDataContex : IUnitOfWork
    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<Customer> Customers { get; set; }

        public void RejectChanges()
        {
            throw new NotImplementedException();
        }

        public DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class
        {
            throw new NotImplementedException();
        }

        public int SaveChanges()
        {
            throw new NotImplementedException();
        }

        IDbSet<TEntity> IUnitOfWork.Set<TEntity>()
        {
            if (typeof(TEntity) == typeof(Order))
            {
                var list = new Common.Test.FakeDbSet<Order>();
                return (IDbSet<TEntity>)list;
            }
            else if (typeof(TEntity) == typeof(Customer))
            {
                var list = new Common.Test.FakeDbSet<Customer>();
                return (IDbSet<TEntity>)list;
            }

            throw new NotImplementedException();
        }
    }
}

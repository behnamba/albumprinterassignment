﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment.Common.Exceptions
{
    public class DuplicateEntity : Exception
    {
        private DuplicateEntity()
        {
        }

        public DuplicateEntity(string message) : base(message)
        {
        }
    }
}
